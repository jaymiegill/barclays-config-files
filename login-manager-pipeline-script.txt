node 
{
   // Mark the code checkout 'stage'....
   stage 'Checkout'

   // Get some code from a GitHub repository
   withCredentials([usernamePassword(credentialsId: 'b0dab3e6-22ae-4db4-a7fd-5dd417a64a6c', passwordVariable: 'myLord43in43@ccan', usernameVariable: 'swright@celestial.co.uk')])
   {
       //git url: 'https://bitbucket.org/stream2stream/loginmanager/'
      
       checkout([$class: 'GitSCM', branches: [[name: '*/master']], doGenerateSubmoduleConfigurations: false, extensions: [], submoduleCfg: [], userRemoteConfigs: [[credentialsId: 'b0dab3e6-22ae-4db4-a7fd-5dd417a64a6c', url: 'https://stream2stream@bitbucket.org/stream2stream/loginmanager.git']]])
   
       // Get the maven tool.
       // ** NOTE: This 'M3' maven tool must be configured
       // **       in the global configuration.           
       //def mvnHome = tool 'M3'
    
       // Mark the code build 'stage'....
       stage 'Build'
       // Run the maven build
       //sh "${mvnhome}/bin/mvn clean install"
       sh "/bin/mvn clean package"
       sh 'cp target/LoginManager-1.0.war target/LoginManager.war'
       sh 'mv target/LoginManager.war /var/jenkins_home/workspace/"Deploy Login Manager"/'

   }
}